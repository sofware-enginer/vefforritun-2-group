import { useState } from 'react';
import './App.css';
import { Carousel } from './Components/Carousel/Carousel';
import { Col } from './Components/Col/Col';
import { DatePicker } from './Components/DatePicker';
import { Modal } from './Components/Modal/Modal';
import { Row } from './Components/Row/Row';
import {CartoonNetworkSpinner} from './Components/CartoonNetworkSpinner/CartoonNetworkSpinner';

function App() {
  const [isModal, setIsModal] = useState({isOpen: true});
  const [dateChange, onDateChange] = useState({date: new Date()});

  useState(() => {
    console.log(dateChange);
  }, [dateChange]);

  return (
    <div className="App">
      <Modal
        isOpen={isModal.isOpen}
        onClose={() => setIsModal({isOpen: false})}
      >
        <Modal.Title>My Modal</Modal.Title>
        <Modal.Body>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
          Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</Modal.Body>
        <Modal.Footer>modal@modal.com - modals</Modal.Footer>
      </Modal>
      <Carousel images={[
        "https://media.istockphoto.com/id/1322277517/photo/wild-grass-in-the-mountains-at-sunset.jpg?s=612x612&w=0&k=20&c=6mItwwFFGqKNKEAzv0mv6TaxhLN3zSE43bWmFN--J5w=",
        "https://media.istockphoto.com/photos/mountain-landscape-picture-id517188688?b=1&k=20&m=517188688&s=612x612&w=0&h=x8h70-SXuizg3dcqN4oVe9idppdt8FUVeBFemfaMU7w=",
        "https://1.bp.blogspot.com/-kK7Fxm7U9o0/YN0bSIwSLvI/AAAAAAAACFk/aF4EI7XU_ashruTzTIpifBfNzb4thUivACLcBGAsYHQ/s1280/222.jpg", 
        "https://img.freepik.com/free-photo/high-angle-shot-city-buildings-new-york-manhattan_181624-24684.jpg",
        "https://gratisography.com/wp-content/uploads/2023/02/gratisography-colorful-kittenfree-stock-photo-800x525.jpg"
      ]}/>

     <Row>
        <Col size={4} />
        <Col size={4} />
        <Col size={4} />
      </Row>
      <Row>
        <Col size={6} />
        <Col size={1} />
      </Row>

      <DatePicker onDatePick={date => onDateChange({date})} locale="en-EN" />
      

      <CartoonNetworkSpinner interval={3} />

    </div>
  );
}

export default App;
