import React from "react";
import style from "./Modal.module.css";
import { GrClose } from "react-icons/gr";

export const Modal = ({ children, isOpen = false, onClose }) => {
    
    return (
        isOpen === true ? (
        <div className={style.modalParent} onClick={() => onClose()}>
            <div className={style.modalContainer} onClick={(event) => event.stopPropagation()}>
                <GrClose className={style.closeIcon} onClick={() => onClose()}/>
                {children}
            </div>
        </div>) : null
    );
}

Modal.Title = ({ children }) => {
    return (
        <div>
    <h2 className={style.modalTitle}>{children}
        <hr className={style.line}/>
    </h2>
    </div>
    );
}

Modal.Body = ({ children }) => {
    return (
        <p className={style.modalBody}>{children}</p>
    );
}

Modal.Footer = ({ children }) => {
    return (
        <p className={style.modalFooter}>{children}</p>
    );
}