import React, { useEffect, useState } from "react";
import style from "./DatePicker.module.css";
import { BsChevronCompactLeft, BsChevronCompactRight, BsChevronLeft, BsChevronRight } from "react-icons/bs";

export const  DatePicker = ({ onDatePick, locale = "is-IS" }) => {
    const date = new Date();
    const [year, setYear] = useState(date.getFullYear());
    const [day, setDay] = useState(date.getDate());
    const [month, setMonth] = useState({ idx: date.getMonth(), name: "" });
    const [selectedDay, setSelectedDate] = useState({ year: year, month: month.idx, day: day});
    const [dates, setDates] = useState([]);
    const dayNames = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];

    useEffect(() => {
        const newDates = [];
        const currDate = new Date(year, month.idx + 1, 0);
        const newMonth = month;
        newMonth.name = currDate.toLocaleDateString(locale, { month: "long" });
        setMonth(newMonth);
        const totalDates = currDate.getDate();
        let dateRow = [0, 0, 0, 0, 0, 0, 0];
        for (let d = 1; d <= totalDates; d++) {
            currDate.setDate(d);
            dateRow[dayNames.indexOf(dayNames[currDate.getDay()])] = d;
            if (dayNames[currDate.getDay()] === "Sat") {
                newDates.push(dateRow);
                dateRow = [0, 0, 0, 0, 0, 0, 0];
            }
        }
        if (!dateRow.every(i => i === 0)) {
            newDates.push(dateRow);
        }
        setDates(newDates);
    }, [year, month, locale]);

    const iterateMonth = (reverse = false) => {
        let newM = reverse === true ? 11 : 0;
        const newMonth = month;
        if (month.idx > 0 && reverse === true) {
            newM = month.idx - 1;
        }
        if (month.idx < 11 && reverse === false) {
            newM = month.idx + 1;
        }
        newMonth.idx = newM;
        const currDate = new Date(year, newM);
        newMonth.name = currDate.toLocaleDateString(locale, { month: "long"});
        setMonth(({ idx: newM, name: newMonth.name }));
    }

    const selectDay = (d) => {
        setDay(d);
        setSelectedDate({ year: year, month: month.idx, day: d});
        onDatePick(new Date(year, month.idx + 1, day));
    }

    const selectedStyle = (d) => {
        if (selectedDay.year === year && selectedDay.month === month.idx && selectedDay.day === d) {
            return true;
        }
        return false;
    }

    return (
        <>
            <div className={style.datePickerContainer}>
                <div className={style.yearContainer}>
                    <button className={style.btn} onClick={() => setYear(y => y - 1)}><BsChevronLeft /></button>
                    <div>{year}</div>
                    <button className={style.btn} onClick={() => setYear(y => y + 1)}><BsChevronRight /></button>
                </div>
                <div className={style.monthContainer}>
                    <button className={style.btn} onClick={() => iterateMonth(true)}><BsChevronCompactLeft /></button>
                    <div>{month.name}</div>
                    <button className={style.btn} onClick={() => iterateMonth()}><BsChevronCompactRight /></button>
                </div>
                <div>
                    <div className={style.dNameContainer}>
                        {dayNames.map(item => <div key={item} className={style.dateItemHeader}>{item[0]}</div>)}
                    </div>
                    {dates.map(items => <div key={items} className={style.dateContainer}>
                            {items.map((item, idx) => 
                                item > 0 ? <div key={idx} className={selectedStyle(item) ? style.selected: style.dateItemMain} onClick={() => selectDay(item)}>{("0" + item).slice(-2)}</div> : <div key={idx} className={style.dateItemHeader}/>
                            )}
                        </div>)}
                </div>
            </div>
        </>
    );
}