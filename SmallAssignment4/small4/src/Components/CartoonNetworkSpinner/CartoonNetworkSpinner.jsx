import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import styles from "./CartoonNetworkSpinner.module.css";

export const CartoonNetworkSpinner = ({ interval = 3 }) => {
  const characters = [
    'https://static.wikia.nocookie.net/adjl/images/8/8f/Jakelonginvaari.jpg',
    'https://static.wikia.nocookie.net/mycun-the-movie/images/5/5a/LBB_Pig.png',
    'https://resizing.flixster.com/ng9pp3PkugM4RJUvGir8dTa8M_o=/fit-in/1152x864/v2/https://flxt.tmsimg.com/assets/p250025_b_v5_ad.jpg',
    'https://static.wikia.nocookie.net/cartooncharacters/images/9/97/Penguin.png',
    'https://static.wikia.nocookie.net/cartooncharacters/images/0/08/BLOSSOM_OF_POWERPUFF_GIRLS.png',
    'https://static.wikia.nocookie.net/cartooncharacters/images/c/c7/BUTTERCUP.png',
    // Add more character image URLs here
  ];

  const [characterIdx, setCharacterIdx] = useState(0);
  const [isRotate, setIsRotate] = useState(false);

  useEffect(() => {
    if(!isRotate) {
      setTimeout(() => {
        setIsRotate(true);
      }, (interval * 1000));
    }
    else if (isRotate) {
      setTimeout(() => {
        setCharacterIdx(prev => prev < characters.length - 1 ?  prev + 1 : 0);
        setIsRotate(false);
      }, 1150);
    }
  }, [isRotate, interval, characters.length]);

  return (
    <div
      className={isRotate ? styles.animation : styles.cartoonBase}
      title="Cartoon Characters"
      style={{backgroundImage: `url(${characters[characterIdx]})`}}
    />
  );
};

CartoonNetworkSpinner.propTypes = {
  interval: PropTypes.number,
};
