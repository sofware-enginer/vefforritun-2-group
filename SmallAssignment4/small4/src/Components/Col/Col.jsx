// Col.js
import React from 'react';
import PropTypes from 'prop-types';
import styles from "./Col.module.css";

export const Col = ({ children, size }) => {
  return (
    <div className={styles.rowContainer} style={{width: `${((size / 12.0) * 100.0)}%` }}>
      {children}
    </div>
  );
};

Col.propTypes = {
  size: PropTypes.number,
};

Col.defaultProps = {
  size: 1,
};
