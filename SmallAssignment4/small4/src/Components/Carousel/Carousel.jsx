import React, { useState } from "react";
import style from "./Carousel.module.css";

export const Carousel = ({images, size = "medium"}) => {

    const [imageIdx, setImageIdx] = useState(0);

    const nextImage = () => {
        if (imageIdx < images.length - 1) {
            setImageIdx(prevIdx => prevIdx + 1);
        } else {
            setImageIdx(0);
        }
    }

    const prevImage = () => {
        if (imageIdx > 0) {
            setImageIdx(prevIdx => prevIdx - 1);
        } else {
            setImageIdx(images.length - 1);
        }
    }

    return (
        <div className={style.carouselContainer}>
            <div className={style[size]} style={{backgroundImage: `url(${images[imageIdx]})`}}/>
            <div className={style.btnContainer}>
                <button className={style.btn} onClick={() => prevImage()}>Previous</button>
                <button className={style.btn} onClick={() => nextImage()}>Next</button>
            </div>
        </div>
    );
}