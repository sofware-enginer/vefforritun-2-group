import React from 'react';
import PropTypes from 'prop-types';

export const Row = ({ children }) => {
  return (
    <div style={{display: "flex", flexWrap: 'wrap'}}>
      {children}
    </div>
  );
};

Row.propTypes = {
  children: PropTypes.node,
};
