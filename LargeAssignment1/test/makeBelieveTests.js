const checkFuncElem = __('.first');

function getElements(obj) {
    if(obj == null) { return -1; }
    if(Object.prototype.toString.call(obj) === '[object Array]') {
        return obj;
    } else if(Object.keys(obj).length){
        for (const property in obj) {
            const theProp = obj[property];
            if(Object.prototype.toString.call(theProp) === '[object Array]' ||
            Object.prototype.toString.call(theProp) === '[object NodeList]') {
                return theProp;
            } else if(Object.prototype.toString.call(theProp) === '[object HTMLDivElement]'){
                return [theProp];
            }
        }
    }
}

function funcExists(obj, func){
    if(typeof obj[func] === 'function') {
        return true;
    }
}

function implementsFunc(obj, func){
    var hasFunc = funcExists(obj, func);
    it(`Should implement ${func}()`, function() { chai.expect(hasFunc).to.be.true; });
    return hasFunc;
}

function checkUndefinedAndElemCount(testMe, elemCount){
    it("Should not be undefined", function() {
        chai.expect(testMe).to.not.be.undefined;
    });
    it("Have two elements selected", function() {
        chai.expect(testMe.length).to.eq(elemCount);
    });
}

chai.should();

describe("#1 Get .first element with query syntax", function() {
    checkUndefinedAndElemCount(getElements(__('.first')), 2);
  });

describe("#2 Parent()", function() {
    if(implementsFunc(checkFuncElem, 'parent')){
        checkUndefinedAndElemCount(getElements(__('.second').parent()), 1);
    }
    
});

describe("#2.1 Parent(query)", function() {
    if(implementsFunc(checkFuncElem, 'parent')){
        checkUndefinedAndElemCount(getElements(__('.second').parent('.first')), 1);
    }
    
});

describe("#2.2 Parent() unknown", function() {
    if(implementsFunc(checkFuncElem, 'parent')){
        checkUndefinedAndElemCount(getElements(__('document').parent()), 0);
    }
    
});

describe("#2.3 Parent(query) unknonw", function() {
    if(implementsFunc(checkFuncElem, 'parent')){
        checkUndefinedAndElemCount(getElements(__('.second').parent('.flesk')), 0);
    }
});

describe("#3 Parent().Parent()", function() {
    if(implementsFunc(checkFuncElem, 'parent')){
        var testMe = null; var success = true;
        try { 
            testMe = getElements(__('.second').parent().parent());
        } catch (error) { 
            success = false; 
        }

        it("Shouldn't throw error", function() {
            chai.expect(success).to.be.true;
        });

        if(success){
            checkUndefinedAndElemCount(testMe, 1);
        }
    }
});

describe("#4 Parent(query)", function() {
    if(implementsFunc(checkFuncElem, 'parent')){
        checkUndefinedAndElemCount(getElements(__('#password').parent('form')), 1);
    }
});

describe("#5 GrandParent()", function() {
    if(implementsFunc(checkFuncElem, 'grandParent')){
        checkUndefinedAndElemCount(getElements(__('#password').grandParent()), 1);
    } 
});

describe("#5.1 GrandParent(query)", function() {
    if(implementsFunc(checkFuncElem, 'grandParent')){
        checkUndefinedAndElemCount(getElements(__('#password').grandParent('.father')), 1);
    }
});

describe("#5.2 GrandParent(query) Unknown", function() {
    if(implementsFunc(checkFuncElem, 'grandParent')){
        checkUndefinedAndElemCount(getElements(__('#password').grandParent('.unknownnnn')), 0);
    }
});

describe("#5.3 Grandparent() - Small P - Ignore if Big P works", function() {
    if(implementsFunc(checkFuncElem, 'grandparent')){
        checkUndefinedAndElemCount(getElements(__('#password').grandparent()), 1);
    }
});

describe("#5.5 Grandparent(query) - Small P - Ignore if Big P works", function() {
    if(implementsFunc(checkFuncElem, 'grandparent')){
        checkUndefinedAndElemCount(getElements(__('#password').grandparent('.father')), 1);
    }
});

describe("#5.6 Grandparent(query) Unknown - Small P - Ignore if Big P works", function() {
    if(implementsFunc(checkFuncElem, 'grandparent')){
        checkUndefinedAndElemCount(getElements(__('#password').grandparent('.unknownnnn')), 0);
    }
});

describe("#6 __('#password').ancestor('.ancestor')", function() {
    if(implementsFunc(checkFuncElem, 'ancestor')){
        checkUndefinedAndElemCount(getElements(__('#password').ancestor('.ancestor')), 1);
    }
});

describe("#6.1 Ancestor(query)", function() {
    if(implementsFunc(checkFuncElem, 'ancestor')){
        checkUndefinedAndElemCount(getElements(__('#password').ancestor('.root')), 1);
    }
});

describe("#6.2 Ancestor(query) Unknown", function() {
    if(implementsFunc(checkFuncElem, 'ancestor')){
        var testMe = null;var success = true;
        try { 
            testMe = getElements(__('#password').ancestor('.ancestor-sibling'));
        } catch (error) { 
            success = false; 
        }

        it("Shouldn't throw error", function() {
            chai.expect(success).to.be.true;
        });
        
        if(success){
            checkUndefinedAndElemCount(testMe, 0);
        }       
    } 
});

describe("#7 OnClick", function() {
    if(implementsFunc(checkFuncElem, 'onClick')){
        __('#btn1').onClick(function(evt) {
            __('.onclick').append('<p class="fifth">Fifth</p>');
            evt.preventDefault();
            return false;
        })

        var formBtn = document.getElementById('btn1');
        formBtn.click();
        checkUndefinedAndElemCount(getElements(__('.onclick')), 1);
    }
});

describe("#8 insertText", function() {
    if(implementsFunc(checkFuncElem, 'insertText')){
        var testMe = null;
        var success = true;
        try { 
            __('.insert-text').insertText('Nei ég er textinn!');
        } catch (error) { 
            success = false; 
        }

        it("Shouldn't throw error", function() {
            chai.expect(success).to.be.true;
        });
        
        if(success){
            testMe = document.getElementsByClassName('insert-text');

            it("Should not be undefined", function() {
                chai.expect(testMe[0].innerText).to.eq('Nei ég er textinn!');
            });
            it("Length should be 0", function() {
                chai.expect(testMe.length).to.be.greaterThan(0);
            });
        }
    }
});

describe("#9 Append", function() {
    if(implementsFunc(checkFuncElem, 'append')){
        var testMe = null; var success = true;
        try { 
            __('.append').append('<p class="appended">Appended 1</p>');
        } catch (error) {  success = false; }

        it("Shouldn't throw error", function() { chai.expect(success).to.be.true; });
        
        if(success){
            checkUndefinedAndElemCount(getElements(__('.appended')), 1);
        }       
    }
});

describe("#9.1 Append created element", function() {
    if(implementsFunc(checkFuncElem, 'append')){
        var testMe = null; var success = true;
        try { 
            var pElem = document.createElement('p');
            pElem.textContent = 'Appended 2';
            pElem.className = 'appended2';
            __('.append').append(pElem);
        } catch (error) {  success = false; }

        it("Shouldn't throw error", function() { chai.expect(success).to.be.true; });
        
        if(success){
            checkUndefinedAndElemCount(getElements(__('.appended2')), 1);
        }       
    }
});

describe("#10 Prepend", function() {
    if(implementsFunc(checkFuncElem, 'prepend')){
        var testMe = null; var success = true;
        try { 
            __('.prepend').prepend('<p class="prepended">Appended 1</p>');
        } catch (error) {  success = false; }

        it("Shouldn't throw error", function() { chai.expect(success).to.be.true; });
        
        if(success){
            checkUndefinedAndElemCount(getElements(__('.prepended')), 1);
        }       
    }
});

describe("#10.1 Prepend created element", function() {
    if(implementsFunc(checkFuncElem, 'prepend')){
        var testMe = null; var success = true;
        try { 
            var pElem = document.createElement('p');
            pElem.textContent = 'Appended 2';
            pElem.className = 'prepended2';
            __('.prepend').prepend(pElem);
        } catch (error) {  success = false; }

        it("Shouldn't throw error", function() { chai.expect(success).to.be.true; });
        
        if(success){
            checkUndefinedAndElemCount(getElements(__('.prepended2')), 1);
        }       
    }
});


describe("#11 Delete", function() {
    if(implementsFunc(checkFuncElem, 'delete')){
        var testMe = null; var success = true;
        try { 
            __('.deleteme').delete();
        } catch (error) {  success = false; }

        it("Shouldn't throw error", function() { chai.expect(success).to.be.true; });
        
        if(success){
            checkUndefinedAndElemCount(getElements(__('.deleteme')), 0);
        }       
    }
});

describe("#11.1 Delete non existent", function() {
    if(implementsFunc(checkFuncElem, 'delete')){
        var testMe = null; var success = true;
        try { 
            __('.deletemeeee').delete();
        } catch (error) {  success = false; }

        it("Shouldn't throw error", function() { chai.expect(success).to.be.true; });
        
        if(success){
            checkUndefinedAndElemCount(getElements(__('.deletemeeee')), 0);
        }       
    }
});

describe("#12 CSS", function() {
    if(implementsFunc(checkFuncElem, 'css')){
        var testMe = null; var success = true;
        try { 
            __('#css').css('background-color', 'black');
        } catch (error) {  success = false; }

        it("Shouldn't throw error", function() { chai.expect(success).to.be.true; });
        
        if(success){
            testMe = document.getElementById('css');
            it("Should not be undefined", function() {
                chai.expect(testMe).to.not.be.undefined;
            });
            it("background-color should be black", function() {
                chai.expect(testMe.style['background-color']).to.be.eq('black');
            });   
        }       
    }
});

describe("#13 ToggleClass", function() {
    if(implementsFunc(checkFuncElem, 'toggleClass')){
        var testMe = null; var success = true;
        try { 
            __('#toggle').toggleClass('jeij');
        } catch (error) {  success = false; }

        it("Shouldn't throw error", function() { chai.expect(success).to.be.true; });
        
        if(success){
            testMe = document.getElementById('toggle');
            it("Should not be undefined", function() {
                chai.expect(testMe).to.not.be.undefined;
            });
            it("Classlist should contain .jeij", function() {
                chai.expect(testMe.classList.contains('jeij')).to.be.true;
            });   
        }       
    }
});

describe("#14 OnSubmit", function() {
    if(implementsFunc(checkFuncElem, 'onSubmit')){
        var testMe = null; var success = true;
        try { 
            __('#form2').onSubmit((evt) =>
            { 
                console.log('SUBMIT!!!!!')
                __('#form2').append('<p id="submitelem">Fredd</p>');
                evt.preventDefault(); 
                return false; 
            });
            //document.getElementById('btn2').click();
            //document.getElementById('form2').submit();
            document.getElementById('form2').dispatchEvent(new Event('submit', {bubbles:false}));
        } catch (error) {  success = false; }

        it("Shouldn't throw error", function() { chai.expect(success).to.be.true; });
        
        if(success){
            checkUndefinedAndElemCount(getElements(__('#submitelem')), 1);
        }       
    }
});

describe("#15 OnInput", function() {
    if(implementsFunc(checkFuncElem, 'onInput')){
        var testMe = null; var success = true;
        try { 
            __('#username').onInput((evt) =>
            { 
                __('#form2').append('<p id="inputtelem"></p>');
            });
            document.getElementById('username').dispatchEvent(new Event('input', {bubbles:true}));
        } catch (error) {  success = false; }

        it("Shouldn't throw error", function() { chai.expect(success).to.be.true; });
        
        if(success){
            checkUndefinedAndElemCount(getElements(__('#inputtelem')), 1);
        }       
    }
});


describe("#16 Ajax", function() {
    beforeEach(function() {
        this.xhr = sinon.useFakeXMLHttpRequest();
        
        this.requests = [];
        this.xhr.onCreate = function(xhr) {
            this.requests.push(xhr);
        }.bind(this);
        });
        
        afterEach(function() {
        this.xhr.restore();
    });

    if(implementsFunc(__, 'ajax')){
        var testMe = null; var success = true;
        try { 
            var data = "OK";
            var dataJson = JSON.stringify(data);
            //console.log(data);
            //console.log(dataJson);
            it('should perform ajax..', function(done) {
                __.ajax({
                    url: 'https://serene-island-81305.herokuapp.com/api/200',
                    method: 'GET',
                    timeout: 10,
                    data: {},
                    headers: [
                        { 'Authorization' : 'my-secret-key'}
                    ],
                    success: function (resp){
                  //      console.log('success', resp);
                        //resp.should.equal(data);
                        done();                        
                    },
                    fail: function (error){ console.log("Error", error) },
                    beforeSend: function (xhr){ console.log("XHR", xhr);},
                })
                //this.requests[0].respond(200);
                this.requests[0].respond(200, { 'Content-Type': 'text/json' }, data);
                this.requests[0].response.should.eq(data);
            });
        } catch (error) {  success = false; }

        it("Shouldn't throw error", function() { chai.expect(success).to.be.true; });        
    }

    
});



describe("#16.1 Ajax", function() {
    beforeEach(function() {
        this.xhr = sinon.useFakeXMLHttpRequest();
        
        this.requests = [];
        this.xhr.onCreate = function(xhr) {
            this.requests.push(xhr);
        }.bind(this);
        });
        
        afterEach(function() {
        this.xhr.restore();
    });

    if(implementsFunc(__, 'ajax')){
        var testMe = null; var success = true;
        try { 
            var data = "Bad Request";
            var dataJson = JSON.stringify(data);
            //console.log(data);
            //console.log(dataJson);
            it('should perform ajax...', function(done) {
                __.ajax({
                    url: 'https://serene-island-81305.herokuapp.com/api/400',
                    method: 'GET',
                    timeout: 10,
                    data: {},
                    headers: [
                        { 'Authorization' : 'my-secret-key'}
                    ],
                    success: function (resp){                     
                    },
                    fail: function (error){ 
                        //error.should.equal(data);
                        done();   
                     },
                    beforeSend: function (xhr){ console.log(xhr);},
                })
                //console.log('BR request', this.requests[0]);
                //this.requests[0].respond(200);
                this.requests[0].respond(400, { 'Content-Type': 'text/json' }, data);
                this.requests[0].response.should.eq(data);
            });
        } catch (error) {  success = false; }

        it("Shouldn't throw error", function() { chai.expect(success).to.be.true; });        
    }

    
});
