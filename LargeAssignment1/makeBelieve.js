/* ADD YOUR JS CODE BELOW */
"use strict";

(function (globalObj) {
    
    function MakeBelieveElement(nodes) {
        this.nodes = nodes;
    };
    
    // TODO: Make parent()
    MakeBelieveElement.prototype.parent = function (selector) {
        var parents = [];
        this.nodes.forEach(element => {
            if(!selector && element.parentNode)
            {
                parents.push(element.parentNode);
            }
            if (selector && element.parentNode && (
                element.parentNode.nodeName.toLowerCase() == selector ||
                element.parentNode.id == selector.substring(1, selector.length) ||
                element.parentNode.className == selector.substring(1, selector.length)
            )) {
                parents.push(element.parentNode);
            }
        });
        this.nodes = parents;
        return this;
    };

    // TODO: Make grandParent()
    MakeBelieveElement.prototype.grandParent = function (selector) {
        return this.parent().parent(selector);
    };

    // need to fix
    
    MakeBelieveElement.prototype.ancestor = function (selector) {
        var ancestors = [];
        var ancestor = this.grandParent();
        while (ancestor.nodes.length > 0) {
            ancestor.nodes.forEach(elem => {
                if (selector && elem.parentNode && elem.parentNode.nodeName != "#document") {
                    var tempSelector = ancestor.nodes;
                    if (ancestor.parent(selector).nodes.length > 0) {
                        ancestors = ancestors.concat(ancestor.nodes);
                    }
                    else {
                        ancestor.nodes = tempSelector;
                        ancestor = ancestor.parent();
                    }
                }
                else {
                    ancestor = ancestor.parent();
                }
            });
        }
        this.nodes = ancestors;
        return this;
    };

    MakeBelieveElement.prototype.onClick = function (callback) {
        this.nodes.forEach(element => {
             element.onclick = callback; 
        });
    };

    MakeBelieveElement.prototype.insertText = function (text) {
        this.nodes.forEach(element => {
            element.innerText = text;
        });
    };

    MakeBelieveElement.prototype.append = function (item) {
        this.nodes.forEach(element => {
            if (typeof item == "object")
            {
                element.innerHTML += "\n" + item.outerHTML;
            }
            else {
                element.innerHTML += "\n" + item;
            }
        });
    };  
    

    MakeBelieveElement.prototype.prepend = function (item) {
        this.nodes.forEach(element => {
            if (typeof item == "object")
            {
                element.innerHTML = item.outerHTML + "\n" + element.innerHTML;
            }
            else {
                element.innerHTML = item + "\n" + element.innerHTML;
            }
        });
    };   

    // need to fix
    MakeBelieveElement.prototype.delete = function () {
        this.nodes.forEach(elem => {
            elem.remove();
        });
    };
    

    query.ajax = function (obj) {
        const url = obj.url;
        const method = obj.method || 'GET';
        const timeout = obj.timeout || 0;
        const data = obj.data || {};
        const headers = obj.headers || {};
        const success = obj.success || null;
        const fail = obj.fail || null;
        const beforeSend = obj.beforeSend || null;

        // if (typeof obj === 'object') {
        // }
        var xhr = new XMLHttpRequest();
        xhr.open(method, url, true);
        xhr.timeout = timeout;
        if (headers.length > 0) {
            headers.forEach(item => {
                xhr.setRequestHeader(Object.keys(item)[0], Object.values(item)[0]);
            });
        };
        xhr.onreadystatechange = function () { 
            if (xhr.readyState == XMLHttpRequest.OPENED) {
                beforeSend();
            }
            else if (xhr.readyState == XMLHttpRequest.DONE) {
                    success(); 
                
                
            }
        };

        xhr.onerror = fail();

        xhr.send(JSON.stringify(data));
    };

    MakeBelieveElement.prototype.onSubmit = function (callback) {
        this.nodes.forEach(element => {
            element.onsubmit = callback; 
       });
    };
    

    MakeBelieveElement.prototype.css = function (attribute, value) {
        this.nodes.forEach(elem => {
            if (elem.style[attribute] != undefined) {
                elem.style[attribute] = value;
            }
        });
    };

    MakeBelieveElement.prototype.toggleClass = function (className) {
    
        this.nodes.forEach(elem => {
            if (elem.classList.contains(className)) {
                    elem.classList.remove(className);
            }
            else {
                    elem.classList.add(className);
            }
        });

        
    };

    MakeBelieveElement.prototype.onInput = function (callback) {
        this.nodes.forEach(element => {
            element.oninput = callback; 
       });
    };
    
    function query(cssSelector) {
        return new MakeBelieveElement(document.querySelectorAll(cssSelector));
    };


    globalObj.__ = query;

}(window));


__.ajax({url: 'http://localhost'});