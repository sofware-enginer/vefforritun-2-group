/**
    Define the shapes
*/

function Shape(position) {
    this.position = position;
};

Shape.prototype.render = function () {};

Shape.prototype.resize = function () {};

Shape.prototype.move = function () {};

Shape.prototype.fill = function () {};

function Rectangle(position, width, height, isFill, color, lineWidth) {
    Shape.call(this, position);
    this.width = width;
    this.height = height;
    this.isFill = isFill;
    this.lineWidth = lineWidth;
    this.color = color;
};

function Circle(position, width, height, isFill, color, lineWidth) {
    Shape.call(this, position)
    this.width = width;
    this.height = height;
    this.isFill = isFill;
    this.lineWidth = lineWidth;
    this.color = color;
}

function Star(position, width, height, isFill, color, lineWidth) {
    Shape.call(this, position);
    this.width = width;
    this.height = height;
    this.isFill = isFill;
    this.lineWidth = lineWidth;
    this.color = color;
}

function Line(position, x, y, color, lineWidth) {
    Shape.call(this, position);
    this.x = x;
    this.y = y;
    this.lineWidth = lineWidth;
    this.color = color;
}
  

function Texts(position, text, font, color) {
    Shape.call(this, position);
    this.text = text;
    this.font = font;
    this.color = color;
}

function Pen(position, posList, color, lineWidth) {
    Shape.call(this, position)
    this.posList = posList;
    this.lineWidth = lineWidth;
    this.color = color;
}
// Assign the prototype
Rectangle.prototype = Object.create(Shape.prototype);
Rectangle.prototype.constructor = Rectangle;

Rectangle.prototype.render = function () {
    // Render a rectangle
    if (this.isFill) {
        drawio.ctx.fillStyle = this.color;
        drawio.ctx.fillRect(this.position.x, this.position.y, this.width, this.height);
    } else {
        drawio.ctx.strokeStyle = this.color;
        drawio.ctx.lineWidth = this.lineWidth;
        drawio.ctx.strokeRect(this.position.x, this.position.y, this.width, this.height);
    }
};

Rectangle.prototype.resize = function (x, y) {
    this.width = x - this.position.x;
    this.height = y - this.position.y;
};

Rectangle.prototype.move = function (mousePos, shapePos) {
    this.position.x = mousePos.x + shapePos.x;
    this.position.y = mousePos.y + shapePos.y;
};

Rectangle.prototype.fill = function (isFill, color) { 
    this.isFill = isFill;
    this.color = color;
};



Circle.prototype = Object.create(Shape.prototype);
Circle.prototype.constructor = Circle;

Circle.prototype.render = function () {
    // Render a circle
    drawio.ctx.beginPath();
    drawio.ctx.ellipse(this.position.x + (this.width * 0.5), this.position.y + (this.height * 0.5), Math.abs(this.width * 0.5), Math.abs(this.height * 0.5), 0, 0, 2 * Math.PI);
    if (this.isFill) {
        drawio.ctx.fillStyle = this.color;
        drawio.ctx.fill();
    } else {
        drawio.ctx.lineWidth = this.lineWidth;
        drawio.ctx.strokeStyle = this.color;
        drawio.ctx.stroke();
    }
    
};
  
Circle.prototype.resize = function (x, y) {
  this.width = x - this.position.x;
  this.height = y - this.position.y;
}

Circle.prototype.move = function (mousePos, shapePos) {
    this.position.x = mousePos.x + shapePos.x;
    this.position.y = mousePos.y + shapePos.y;
};

Circle.prototype.fill = function (isFill, color) { 
    this.isFill = isFill;
    this.color = color;
};



Star.prototype.render = function () {
    // Render a star
    drawio.ctx.beginPath();

    drawio.ctx.moveTo(this.position.x + (this.width * 0.5), this.position.y); 
    drawio.ctx.lineTo(this.position.x + (this.width * 0.6), this.position.y + (this.height * (0.35))); // 
    //first arm
    drawio.ctx.lineTo(this.position.x + this.width, this.position.y + (this.height * (0.38))); // 
    drawio.ctx.lineTo(this.position.x + (this.width * 0.7), this.position.y + (this.height * (0.6))); // 
    //second arm
    drawio.ctx.lineTo(this.position.x + (this.width * 0.8), this.position.y + this.height); // 
    drawio.ctx.lineTo(this.position.x + (this.width * 0.5), this.position.y + (this.height * (0.8))); 
    //third arm
    drawio.ctx.lineTo(this.position.x + (this.width * 0.2), this.position.y + this.height); // 
    drawio.ctx.lineTo(this.position.x + (this.width * 0.3), this.position.y + (this.height * (0.6))); // 
    //forth arm
    drawio.ctx.lineTo(this.position.x, this.position.y + (this.height * (0.38))); // 
    drawio.ctx.lineTo(this.position.x + (this.width * 0.4), this.position.y + (this.height * (0.35))); // 
    // close fifth arm
    drawio.ctx.lineTo(this.position.x + (this.width * 0.5), this.position.y); 
    if (this.isFill) {
        drawio.ctx.fillStyle = this.color;
        drawio.ctx.fill();
    } else {
        drawio.ctx.lineWidth = this.lineWidth;
        drawio.ctx.strokeStyle = this.color;
        drawio.ctx.stroke();
    }
};

Star.prototype.resize = function (x, y) {
    this.width = x - this.position.x;
    this.height = y - this.position.y;
};

Star.prototype.move = function (mousePos, shapePos) {
    this.position.x = mousePos.x + shapePos.x;
    this.position.y = mousePos.y + shapePos.y;
};

Star.prototype.fill = function (isFill, color) { 
    this.isFill = isFill;
    this.color = color;
};

Pen.prototype = Object.create(Shape.prototype);
Pen.prototype.constructor = Pen;

Pen.prototype.render = function () {
    drawio.ctx.beginPath();
    drawio.ctx.strokeStyle = this.color;
    drawio.ctx.lineWidth = this.lineWidth;

    drawio.ctx.moveTo(this.position.x, this.position.y);
    
    if (this.posList) {
        this.posList.forEach(elem => {
            drawio.ctx.lineTo(elem.x, elem.y);
        });
    }
    drawio.ctx.stroke();    
};

Pen.prototype.resize = function (x, y) {
    // Add a new point to the array of points
    this.posList.push({x: x, y: y});
};

Texts.prototype = Object.create(Shape.prototype);
Texts.prototype.constructor = Text;
//TODO add a working
Texts.prototype.render = function () {
    drawio.ctx.font = `24px ${this.font}`
    drawio.ctx.fillStyle = this.color;
    drawio.ctx.fillText(this.text, this.position.x, this.position.y * 1.085)
};

Texts.prototype.move = function (mousePos, shapePos) {
    this.position.x = mousePos.x + shapePos.x;
    this.position.y = mousePos.y + shapePos.y;
};

Texts.prototype.getSize = function () {
    let metrics = drawio.ctx.measureText(text);
    return {width: 100, height: metrics.actualBoundingBoxAscent + metrics.actualBoundingBoxDescent};
};

Line.prototype = Object.create(Shape.prototype);
Line.prototype.constructor = Line;

Line.prototype.render = function() {
    drawio.ctx.beginPath(); 
    drawio.ctx.strokeStyle = this.color;
    drawio.ctx.lineWidth = this.lineWidth;
    drawio.ctx.moveTo(this.position.x, this.position.y);
    drawio.ctx.lineTo(this.x, this.y);
    drawio.ctx.stroke();
};

Line.prototype.resize = function(x, y) {
    this.x = x;
    this.y = y;
};

Line.prototype.move = function(mousePos, shapeStartPos, shapeEndPos) {
    this.position.x = mousePos.x + shapeStartPos.x;
    this.position.y = mousePos.y + shapeStartPos.y;
    this.x = mousePos.x + shapeEndPos.x;
    this.y = mousePos.y + shapeEndPos.y;
};
