function generateShapes (shapes, availableShapes) {
    const newShape = new Array();
    shapes.forEach((shape) => {
        const shapeKey = Object.keys(shape)[0]
        const shapeValue = Object.values(shape)[0];
        let shapeObj = {};
        switch (shapeKey) {
            case availableShapes.RECTANGLE:
                shapeObj[availableShapes.RECTANGLE] = new Rectangle(shapeValue.position, shapeValue.width, shapeValue.height, shapeValue.isFill, shapeValue.color, shapeValue.lineWidth);
                newShape.push(shapeObj);
                break;
            case availableShapes.CIRCLE:
                shapeObj[availableShapes.CIRCLE] = new Circle(shapeValue.position, shapeValue.width, shapeValue.height, shapeValue.isFill, shapeValue.color, shapeValue.lineWidth);
                newShape.push(shapeObj);
                break;
            case availableShapes.PEN:
                shapeObj[availableShapes.PEN] = new Pen(shapeValue.position, shapeValue.podList, shapeValue.color, shapeValue.lineWidth);
                newShape.push(shapeObj);
                break;
            case availableShapes.LINE:
                shapeObj[availableShapes.LINE] = new Line(shapeValue.position, shapeValue.end, shapeValue.color, shapeValue.lineWidth);
                newShape.push(shapeObj);
                break;
            case availableShapes.TEXT:
                shapeObj[availableShapes.TEXT] = new Texts(shapeValue.position, shapeValue.text, shapeValue.font, shapeValue.color);
                newShape.push(shapeObj);
                break;
            case availableShapes.STAR:
                shapeObj[availableShapes.STAR] = new Star(shapeValue.position, shapeValue.width, shapeValue.height, shapeValue.isFill, shapeValue.color, shapeValue.lineWidth);
                newShape.push(shapeObj);
                break;
        }
    });
    return newShape;
}

function loadShapes (drawio) {
    drawio.shapes = generateShapes(drawio.shapes, drawio.availableShapes);
    drawio.canvas = document.getElementById('my-canvas');
    drawio.selectedElement = null;
    drawio.ctx = document.getElementById('my-canvas').getContext('2d');
    drawio.selectedShape = drawio.availableShapes.PEN;
    return drawio;
}

function getLocalStorage(key) {
    let getStorage = localStorage.getItem(key);
    let drawio = {};
    if (getStorage) {
        drawio = loadShapes(JSON.parse(getStorage));
    }
    return drawio;
};

function saveLocalStorage(key, str) {
    localStorage.setItem(key, str);
};