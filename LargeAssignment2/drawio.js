/**
    Structure for the assignment 2 project
*/

// 1. Define a function namespace called drawio

// 2. Create an array to hold on to the shapes currently drawn


var LINESIZE = document.getElementById('quantity').value;
var FONT = document.getElementById('font').value; 
var COLOR = document.getElementById('body').value;


const newDrawio = {
    shapes: [],
    redoShapes: [],
    selectedShape: 'pen',
    canvas: document.getElementById('my-canvas'),
    ctx: document.getElementById('my-canvas').getContext('2d'),
    isMoving: false,
    isFill: false,
    isWriting: false,
    selectedElement: null,
    availableShapes: {
        RECTANGLE: 'rectangle',
        CIRCLE: 'circle',
        STAR: 'star',
        LINE: 'line',
        TEXT: 'text',
        PEN: 'pen',
    }
};


// function getLocalStorage () {
//     let localSave = localStorage.getItem('drawio');
//     if (localSave) {
//         localSave = JSON.parse(localSave);
//         localSave.canvas = document.getElementById('my-canvas');
//         localSave.ctx = document.getElementById('my-canvas').getContext('2d');
//     }
//     return localSave;
// }

var test = getLocalStorage('drawio');

window.drawio = Object.values(test).length > 0 ? test : newDrawio;



$(function () {
    // Document is loaded and parsedselectedshape
    drawCanvas();
    let shapePosDis = {x: 0, y: 0};
    let shapeEndDis = {x: 0, y: 0};

    function textInput (pos) {
        const canvas = document.getElementById('my-canvas');
        const canvasLocation = canvas.getBoundingClientRect();
        if(document.getElementsByTagName('input').length <= 2) {
            const textField = document.createElement('input');
            textField.setAttribute('type', 'text');
            textField.setAttribute('class', 'textfield');
            textField.style.cssText = `
                left: ${canvasLocation.x + pos.x}px;
                top: ${canvasLocation.y + pos.y}px;
                font-family: ${FONT};
                color: ${COLOR};
                font-size: 20px;
            `
            textField.addEventListener('focusout', function (e) { 
                drawio.selectedElement = new Texts(pos, e.target.value, FONT, COLOR);
                drawio.selectedElement.render();
                e.target.remove();
            });
            canvas.parentNode.appendChild(textField);
            setTimeout(() => textField.focus(), 0);
        }
    } 

    function distance(x1, y1, x2, y2) {
        let a = Math.abs(x1 - x2);
        let b = Math.abs(y1 - y2);
        return Math.floor(Math.sqrt( a*a + b*b ));
    }

    function drawCanvas() {
        drawio.ctx.clearRect(0, 0, drawio.canvas.width, drawio.canvas.height);
        if (drawio.selectedElement) {
            drawio.selectedElement.render();
        }
        for (var i = 0; i < drawio.shapes.length; i++) {
            Object.values(drawio.shapes[i])[0].render();
        }
    };

    function newShape (selectedShape, pos) {
        switch (selectedShape) {
            case drawio.availableShapes.RECTANGLE:
                drawio.selectedElement = new Rectangle(pos, 0, 0, drawio.isFill, COLOR, LINESIZE);
                break;
            case drawio.availableShapes.CIRCLE:
                drawio.selectedElement = new Circle(pos, 0, 0, drawio.isFill, COLOR, LINESIZE);
                break;
            case drawio.availableShapes.PEN:
                drawio.selectedElement = new Pen(pos, [], COLOR, LINESIZE);
                break;
            case drawio.availableShapes.LINE:
                drawio.selectedElement = new Line(pos, 0, 0, COLOR, LINESIZE);
                break;
            case drawio.availableShapes.TEXT:
                textInput(pos);
                break;
            case drawio.availableShapes.STAR:
                drawio.selectedElement = new Star(pos, 0, 0, drawio.isFill, COLOR, LINESIZE);
                break;
        }
    }

    function selectShape (shape, pos) {

        if (shape instanceof Line && drawio.isMoving) {
            let distance1 = distance(shape.position.x, shape.position.y, pos.x, pos.y);
            let distance2 = distance(shape.x, shape.y, pos.x, pos.y);
            let distance3 = distance(shape.position.x, shape.position.y, shape.x, shape.y);
            if (distance1 + distance2 == distance3) { 
                drawio.selectedElement = shape;
                shapeEndDis.x = shape.x - pos.x;
                shapeEndDis.y = shape.y - pos.y;
            }
            
        } else if (shape instanceof Texts) { 
            let textSize = shape.getSize();
            if ((pos.x <= (shape.position.x + (textSize.width)) && pos.x >= shape.position.x ) && (pos.y <= (shape.position.y + textSize.height) && pos.y >= shape.position.y)) { 
                drawio.selectedElement = shape;
            }
        } else {
            if (shape.width < 0 && shape.height < 0) {
                if (shape && ((pos.x <= shape.position.x && pos.x >= shape.position.x + shape.width) && (pos.y <= shape.position.y && pos.y >= shape.position.y + shape.height))) {
                    drawio.selectedElement = shape;
                }
            }
            else if (shape.width >= 0 && shape.height >= 0) {
                if (shape && ((pos.x >= shape.position.x && pos.x <= shape.position.x + shape.width) && (pos.y >= shape.position.y && pos.y <= shape.position.y + shape.height))) {
                    drawio.selectedElement = shape;
                }
            }
            else if(shape.width < 0) {
                if (shape && ((pos.x <= shape.position.x && pos.x >= shape.position.x + shape.width) && (pos.y >= shape.position.y && pos.y <= shape.position.y + shape.height))) {
                    drawio.selectedElement = shape;
                }
            }
            else if(shape.height < 0) {
                if (shape && ((pos.x >= shape.position.x && pos.x <= shape.position.x + shape.width) && (pos.y <= shape.position.y && pos.y >= shape.position.y + shape.height))) {
                    drawio.selectedElement = shape;
                }
            }
        }
    };
 
    $('.icon').on('click', function () {
        $('.icon').removeClass('selected');
        $(this).addClass('selected');
        drawio.isMoving = $(this).is('#move') ? true : false;
        drawio.isFill = $(this).is('#fill') ? true : false;
        drawio.selectedShape = $(this).data('shape');
    });

    // mousedown
    $('#my-canvas').on('mousedown', function (mouseEvent) {
        var pos = { x: mouseEvent.offsetX, y: mouseEvent.offsetY };
        if (drawio.selectedShape && !drawio.isMoving) {
            newShape(drawio.selectedShape, pos);
        } else {
            /*
                for loop through reversed shallow copy of drawio.shapes,
                so the newest one to move is priority in case of overlap between shapes.
            */
            for (let i = drawio.shapes.length - 1; i >= 0; i--) {
                let shape = Object.values(drawio.shapes[i])[0];
                selectShape(shape, pos)
                if (drawio.selectedElement) {
                    drawio.shapes.splice(i, 1);
                    if (drawio.isMoving) {
                        shapePosDis.x = shape.position.x - pos.x;
                        shapePosDis.y = shape.position.y - pos.y;
                    } else if (drawio.isFill) {
                        drawio.selectedElement.fill(drawio.isFill, COLOR);
                    } 
                    break;
                } 
            }
        }
    });

    // mousemove
    $('#my-canvas').on('mousemove', function (mouseEvent) {
        const pos = {x: mouseEvent.offsetX, y: mouseEvent.offsetY};
        if (drawio.selectedElement && !drawio.isFill) {
            if (!drawio.isMoving) {
                drawio.selectedElement.resize(mouseEvent.offsetX, mouseEvent.offsetY);
            } else if (drawio.isMoving && (drawio.selectedElement instanceof Line)) {
                drawio.selectedElement.move(pos, shapePosDis, shapeEndDis);
            } else if (drawio.isMoving) {
                drawio.selectedElement.move(pos, shapePosDis);
            }
        }
        drawCanvas();
    });

    // mouseup
    $('#my-canvas').on('mouseup', function () {
        if (drawio.selectedElement) {
            drawio.redoShapes = [];
            let shapeObj = {};
            shapeObj[drawio.selectedShape] = drawio.selectedElement;
            drawio.shapes.push(shapeObj);
            drawio.selectedElement = null;
        }
        drawCanvas();
    });

    $('#undo').on('mousedown', function (e) {
        if (drawio.shapes.length > 0) {
            drawio.redoShapes.push(drawio.shapes.pop());
            drawCanvas();
        }
    });

    $('#redo').on('mousedown', function () {
        if (drawio.redoShapes.length > 0) {
            drawio.shapes.push(drawio.redoShapes.pop());
            drawCanvas();
        }
    });

    $('#clear').on('mousedown', function () {
        drawio.shapes = [];
        drawCanvas();
    });

    $('#save').on('mousedown', function () {
        drawio.isMoving = false,
        drawio.isFill = false,
        drawio.isWriting = false,
        saveLocalStorage('drawio', JSON.stringify(drawio));
    });

    $('#body').change(function () {
        COLOR = body.value;      
    });

    $('#font').change(function () {
        FONT = font.value;      
    });

    $('#quantity').change(function () {
        number = parseInt(quantity.value);
        LINESIZE = number;
    });  
});