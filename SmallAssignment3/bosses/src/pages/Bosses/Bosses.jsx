import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { BossBoard } from "../../components/BossBoard/BossBoard";
import { CreateModal } from "../../components/CreateModal/CreateModal";
import { fetchBosses } from "../../slices/BossesSlice";
import "./styles.css";

export const Bosses = () => {
    
    const dispatch = useDispatch();

    const bossesList = useSelector(state => state.bosses.data);

    useEffect(() => {
        dispatch(fetchBosses());
    }, [dispatch]);
    
    return (
        <div className="bossContainer">
            <CreateModal />
            {bossesList.map((bossItem) => {
                return <BossBoard key={bossItem.id} id={bossItem.id} name={bossItem.name} description={bossItem.description} image={bossItem.img}/>
            })}
        </div>
    );
};