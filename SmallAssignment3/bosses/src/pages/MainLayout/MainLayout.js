import { Outlet } from "react-router-dom";
import { NavigationBar } from "../../components/NavigationBar/NavigationBar";

function MainLayout() {

    return (
        <>
            <NavigationBar />
            <div className="page">
                <Outlet />
            </div>
        </>
    );
}

export default MainLayout;
