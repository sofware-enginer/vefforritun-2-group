import { Button } from "@mui/material";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { UpdateModal } from "../../components/UpdateModal/UpdateModal";
import { fetchSingleBoss, removeBoss } from "../../slices/BossItemSlice";
import "./styles.css";

export const BossesDetail = () => {
    const { bossId } = useParams();
    const dispatch = useDispatch();
    const bossItem = useSelector(state => state.singleBoss.data);
    const [isDeleted, setIsDeleted] = useState(false);

    useEffect(() => {
        if (!isDeleted) {
            dispatch(fetchSingleBoss(bossId));
        }
    }, [dispatch, bossId, isDeleted]);

    const onDelete = () => {
        dispatch(removeBoss(bossId))
        setIsDeleted(true);
    };

    return (
        <div className="bossDetailContainer">
            { !isDeleted ?
                <>
                    <div className="bossItemContainer">
                        <img src={bossItem.img} alt={bossItem.name} />
                        <div className="singleBossInfoContainer">
                            <h1>{bossItem.name}</h1>
                            <p>{bossItem.description}</p>
                        </div>
                    </div>
                    <div className="buttonContainer">
                        <Button variant="outlined" onClick={() => onDelete()}>Delete</Button>
                        <UpdateModal id={bossItem.id} name={bossItem.name} description={bossItem.description} img={bossItem.img} />
                    </div>
                </> : <h1>This Boss has been deleted!</h1>}
        </div>
    );
}