import "./styles.css";

export const Home = () => {
    return (
        <div className="mainPageContainer">
            <h1>Welcome to Megaman wiki</h1>
            <p>
                The ultimate resource for all things related to the iconic video game character Megaman!<br />
                Whether you're a long-time fan or just discovering the blue bomber for the first time, our wiki has everything you need to know about the franchise, 
                including detailed information on each game, the characters, and the lore.
            </p>
        </div>
    );
};
