import { configureStore } from "@reduxjs/toolkit";
import BossesSlice from "./slices/BossesSlice";
import BossItemSlice from "./slices/BossItemSlice";

export default configureStore({
    reducer: {
        bosses: BossesSlice,
        singleBoss: BossItemSlice
    }
});