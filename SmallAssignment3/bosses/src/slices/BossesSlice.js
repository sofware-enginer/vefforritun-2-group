import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { postBoss, retrieveAllBosses } from "../services/bossesService";
import { removeBoss, updateSingleBoss } from "./BossItemSlice";

export const fetchBosses = createAsyncThunk("getBosses", async () => {
    const response = await retrieveAllBosses();
    return response;
});

export const postBosses = createAsyncThunk("postBosses", async (data) => {
    const response = await postBoss(data);
    const newItem = {
        id: response.id,
        ...data
    }
    return newItem;
});


const BossesSlice = createSlice({
    name: "bosses",
    initialState: {
        data: [],
        state: "",
        error: ""
    },
    extraReducers: (builder) => {
        /* Builder for fetching data */
        builder
            .addCase(fetchBosses.pending, (state, action) => {
                state.status = 'loading';
            })
            .addCase(fetchBosses.fulfilled, (state, action) => {
                state.status = 'succeeded';
                state.data = action.payload;
            })
            .addCase(fetchBosses.rejected, (state, action) => {
                state.status = 'failed';
                state.error = action.error.message;
            })
            /* Builder for posting new data */
            .addCase(postBosses.pending, (state, action) => {
                state.status = 'loading';
            })
            .addCase(postBosses.fulfilled, (state, action) => {
                state.status = 'succeeded';
                state.data.push(action.payload);
            })
            .addCase(postBosses.rejected, (state, action) => {
                state.status = 'failed';
                state.error = action.error.message;
            })
            .addCase(updateSingleBoss.fulfilled, (state, action) => {
                state.status = 'succeeded';
                state.data = state.data.map(item => item.id === action.payload.id ? action.payload : item);
            })
            .addCase(removeBoss.fulfilled, (state, action) => {
                state.status = 'succeeded';
                state.data = state.data.filter(item => item.id !== action.payload);
            });
    }
});

export default BossesSlice.reducer;