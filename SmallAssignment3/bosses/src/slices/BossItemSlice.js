import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { deleteBoss, getSingleBoss, updateBoss } from "../services/bossesService";


export const fetchSingleBoss = createAsyncThunk("getBoss", async (id) => {
    const response = await getSingleBoss(id);
    return response;
});

export const updateSingleBoss = createAsyncThunk("patchBoss", async ({id, updatedItem}) => {
    await updateBoss(id, updatedItem);
    return {id: id, ...updatedItem};
});

export const removeBoss = createAsyncThunk("removeBosses", async (id) => {
    await deleteBoss(id);
    return id;
});


const SingleBossesSlice = createSlice({
    name: "singleBoss",
    initialState: {
        data: {},
        state: "",
        error: ""
    },
    extraReducers: (builder) => {
        /* Builder for fetching data */
        builder
            .addCase(fetchSingleBoss.pending, (state, action) => {
                state.status = 'loading';
            })
            .addCase(fetchSingleBoss.fulfilled, (state, action) => {
                state.status = 'succeeded';
                // Add any fetched posts to the array
                state.data = action.payload;
            })
            .addCase(fetchSingleBoss.rejected, (state, action) => {
                state.status = 'failed';
                state.error = action.error.message;
            })
            .addCase(updateSingleBoss.pending, (state, action) => {
                state.status = 'loading';
            })
            .addCase(updateSingleBoss.fulfilled, (state, action) => {
                state.status = 'succeeded';
                state.data = action.payload;
            })
            .addCase(updateSingleBoss.rejected, (state, action) => {
                state.status = 'failed';
                state.error = action.error.message;
            })
            .addCase(removeBoss.pending, (state, action) => {
                state.status = 'loading';
            })
            .addCase(removeBoss.fulfilled, (state, action) => {
                state.status = 'succeeded';
                state.data = {};
                
            })
            .addCase(removeBoss.rejected, (state, action) => {
                state.status = 'failed';
                state.error = action.error.message;
            });
    }
});

export default SingleBossesSlice.reducer;