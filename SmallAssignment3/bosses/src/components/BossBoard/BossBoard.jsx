import { Button } from "@mui/material";
import PropType from "prop-types";
import { Link } from "react-router-dom";
import "./styles.css";

export const BossBoard = ({
    id,
    name,
    description,
    image,
}) => (
    <div className="boardContainer">
        <img src={image} alt={name}/>
        <div>
            <h1>{name}</h1>
            <p>{description}</p>
            <Button size="small">
                <Link to={`${id}`} style={{ textDecoration: 'none' }}>More...</Link>
            </Button>
        </div>
    </div>
    );

BossBoard.propType = {
    id: PropType.number.isRequired,
    name: PropType.string.isRequired,
    description: PropType.string.isRequired,
    image: PropType.string.isRequired,
}