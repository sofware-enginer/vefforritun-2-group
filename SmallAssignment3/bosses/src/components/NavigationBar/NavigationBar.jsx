import { NavigationLink } from "../NavigationLink/NavigationLink";
import "./styles.css";

export const NavigationBar = () => {
    return (
        <div className="navigation-bar">
            <ul className="navigation-links">
                <NavigationLink title="Home" href="/" />
                <NavigationLink title="Bosses" href="bosses" />
            </ul>
        </div>
    );
}