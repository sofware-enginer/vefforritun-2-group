import { Modal, Box, Button, TextField, FormControl } from '@mui/material';
import { useRef, useState } from 'react';
import { useDispatch } from 'react-redux';
import validator from "validator"
import { postBosses } from '../../slices/BossesSlice';

const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 500,
    bgcolor: "background.paper",
    borderRadius: 10,
    boxShadow: "0 0 10px 3px #70A4EF",
    pl: 4,
    pr: 4,
    pb: 4,
};

export const CreateModal = () => {
    const dispatch = useDispatch();
    const [open, setOpen] = useState(false);
    const [errors, setErrors] = useState({});

    const nameRef = useRef("");
    const descRef = useRef("");
    const imgRef = useRef("");

    const isValid = () => {
        const name = nameRef.current.value;
        const description = descRef.current.value;
        const img = imgRef.current.value;
    
        const errors = {};
    
        if (name === "") {
          errors.name = "Name is required.";
        }

        if (description === "") {
            errors.description = "Description is required.";
        }

        if (img === "") {
            errors.img = "Image URL is required.";
        }
        else if (!validator.isURL(img)) {
          errors.img = "Image URL is not a valid URL.";
        }

        setErrors(errors);
    
        return Object.keys(errors).length === 0;
    };

    const onSubmit = (e) => {
        e.preventDefault();
        if (!isValid()) {
            return;
        }
        const newItem = {
            name: nameRef.current.value,
            description: descRef.current.value,
            img: imgRef.current.value
        }
        dispatch(postBosses(newItem));
        setOpen(false);

    }
    
    return (
        <div style={{marginLeft: "5%", marginTop: "2%"}}>
            <Button variant="outlined" onClick={() => setOpen(true)} sx={{ml: "auto"}}>Add boss</Button>
            <Modal
                open={open}
                onClose={() => setOpen(false)}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <h1>New Boss:</h1>
                    <form onSubmit={onSubmit}>
                            <FormControl sx={{mb: 2}} fullWidth>
                                <TextField
                                    autoFocus
                                    id="name"
                                    label="Name"
                                    variant="standard"
                                    inputRef={nameRef}
                                    error={Boolean(errors.name)}
                                    helperText={errors.name}
                                />
                            </FormControl>
                            <FormControl sx={{mb: 2}} fullWidth>
                                <TextField
                                    autoFocus
                                    id="desc"
                                    label="Description"
                                    variant="standard"
                                    inputRef={descRef}
                                    error={Boolean(errors.description)}
                                    helperText={errors.description}
                                    multiline
                                />
                            </FormControl>
                            <FormControl sx={{mb: 2}} fullWidth>
                                <TextField
                                    autoFocus
                                    id="img"
                                    label="Image URL"
                                    variant="standard"
                                    inputRef={imgRef}
                                    error={Boolean(errors.img)}
                                    helperText={errors.img}
                                />
                            </FormControl>
                        <Button sx={{float: "right"}} variant="outlined" type="submit">
                            Create
                        </Button>
                    </form>
                </Box>
            </Modal>
        </div>
    );
}