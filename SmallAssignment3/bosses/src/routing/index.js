import { createBrowserRouter, createRoutesFromElements, Route } from 'react-router-dom';
import { Bosses } from '../pages/Bosses/Bosses';
import { BossesDetail } from '../pages/BossesDetail/BossesDetail';
import { Home } from '../pages/Home/Home';
import MainLayout from '../pages/MainLayout/MainLayout';

const router = createBrowserRouter(createRoutesFromElements(
  <Route element={<MainLayout />}>
    <Route path="/" element={<Home />}/>
    <Route path="bosses" element={<Bosses/>} />
    <Route path="bosses/:bossId" element={<BossesDetail />} />
  </Route>
));

export default router;