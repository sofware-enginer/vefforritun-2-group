const hostUrl = "http://localhost:4500/api";

export async function retrieveAllBosses() {
    const response = await fetch(`${hostUrl}/bosses`);
    const result = await response.json();
    return result;
}

export async function getSingleBoss(id) {
    const response = await fetch(`${hostUrl}/bosses/${id}`);
    const result = await response.json();
    return result;
}

export async function postBoss(item) {
    const response = await fetch(`${hostUrl}/bosses`, {
                method: "POST",
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(item),
            });
    const result = await response.json();
    return result;
}


export async function updateBoss(id, item) {
    await fetch(`${hostUrl}/bosses/${id}`, {
                method: "PATCH",
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(item),
            });
}


export async function deleteBoss(id) {
    await fetch(`${hostUrl}/bosses/${id}`, {
        method: "DELETE"
    });
}

