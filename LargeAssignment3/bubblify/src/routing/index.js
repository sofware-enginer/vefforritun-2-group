import { createBrowserRouter, createRoutesFromElements, Route } from 'react-router-dom';
import { About } from '../pages/BubbleAboutMe/BubbleAboutMe';
import { BundleProducts } from '../pages/BubbleBundle/BubbleBundle';
import { BubbleCart } from '../pages/BubbleCart/BubbleCart';
import { BubbleCheckout } from '../pages/BubbleCheckout/BubbleCheckout';
import { BubbleConfirmation } from '../pages/BubbleConfirmation/BubbleConfirmation';
import { BubbleHomePage } from '../pages/BubbleHomePage/BubbleHomePage';
import { BubbleProducts } from '../pages/BubbleProduct/BubbleProduct';
import { BubbleProductDetails } from '../pages/BubbleProductDetail/BubbleProductDetail';
import { MainLayout } from '../pages/layout/MainLayout';


const router = createBrowserRouter(createRoutesFromElements(
    <Route path="/" element={<MainLayout/>}>
        <Route path="" element={<BubbleHomePage />}/>
        <Route path="bubbles" element={<BubbleProducts/>}/>
        <Route path="bubbles/:bubblesItemId" element={<BubbleProductDetails/>}/>
        <Route path="bundles" element={<BundleProducts/>}/>
        <Route path="about" element={<About/>}/>
        <Route path="cart" element={<BubbleCart/>}/>
        <Route path="checkout" element={<BubbleCheckout/>}/>
        <Route path="confirm" element={<BubbleConfirmation />}/>
    </Route>
));

export default router;