import './styles.css';
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types';

export const NavigationLink = ({
    title,
    href
}) => {
    return (
        <li className="navigation-link">
            <Link to={href} className="navLink">{title}</Link>
        </li>
    );
}

NavigationLink.prototype = {
    // The title of the navigation to navigate to
    title: PropTypes.string.isRequired,
    // The href url of the navigation to navigate to
    href: PropTypes.string.isRequired
}