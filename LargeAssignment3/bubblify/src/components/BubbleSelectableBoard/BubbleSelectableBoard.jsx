import { useEffect, useState, useCallback } from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import CheckBoxOutlineBlankIcon from '@mui/icons-material/CheckBoxOutlineBlank';
import CheckBoxIcon from '@mui/icons-material/CheckBox';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import './styles.css';


export const BubbleSelectableBoard = ({
  id,
  name,
  description,
  price,
  image,
  onCounterChange,
}) => {
  const boxShadow = { boxShadow: "0px 0px 20px 5px #F40D30" };
  const [select, setSelect] = useState(false);

  const storeItem = useCallback(() => {
    const cartStorage = localStorage.getItem("userInfo");
    const userInfo = cartStorage ? JSON.parse(cartStorage) : { items: [] };

    if (userInfo.items !== undefined) {
      if (select === false) {
        if (!userInfo.items.some(item => item.id === id)) {
          userInfo.items.push({ id: id, name: name, description: description, price: price, image: image });
        }
      }
      else {
        if (userInfo.items.some(item => item.id === id)) {
          const index = userInfo.items.map(item => item.id).indexOf(id);
          userInfo.items.splice(index, 1);
        }
      }
      onCounterChange(userInfo.items.length);
      userInfo["totalPrice"] = userInfo.items.reduce((total, item) => total + item.price, 0);
    }

    localStorage.setItem("userInfo", JSON.stringify(userInfo));
  }, [id, name, description, price, image, select, onCounterChange]);


  useEffect(() => {
    const cartStorage = localStorage.getItem("userInfo");
    const userInfo = cartStorage ? JSON.parse(cartStorage) : { items: [] };
    if (userInfo.items !== undefined) {
      if (userInfo.items.some(item => item.id === id)) {
        setSelect(true);
      }
      else {
        setSelect(false);
      }
      onCounterChange(userInfo.items.length);
    }
  }, [id, select, onCounterChange, storeItem]);



  return (
    <Card sx={[{ width: 250, margin: 2 }, select && boxShadow ]} onClick={storeItem}>
      { select ? <CheckBoxIcon sx={{color: "#FF6565"}}/> : <CheckBoxOutlineBlankIcon sx={{color: "#FF6565"}}/>}
      <img src={image} alt={name} />
      <CardContent>
        <Typography gutterBottom variant="h5" component="div">
          {name}
        </Typography>
        <Typography variant="body2" color="text.secondary">
          Price: {price ? price : 0}
        </Typography>
      </CardContent>
      <CardActions>
        <Button size="small">
          <Link to={`/bubbles/${id}`} style={{ textDecoration: 'none' }}>More details...</Link>
        </Button>
      </CardActions>
    </Card>
  );
}

BubbleSelectableBoard.propTypes = {
  // The id of the bubble product
  id: PropTypes.number.isRequired,
  // The name of the bubble product
  name: PropTypes.string.isRequired,
  // The description of the bubble product
  description: PropTypes.string.isRequired,
  // The price of the bubble product
  price: PropTypes.number.isRequired,
  // The image URL of the bubble product
  image: PropTypes.string.isRequired,
  // The setState is called which is called when storeItem function is called 
  // and it's the counter for products that are selected
  onCounterChange: PropTypes.func.isRequired,
}

