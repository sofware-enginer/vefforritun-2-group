import { NavigationLink } from '../NavigationLink/NavigationLink';
import './styles.css';
import Logo from '../../resource/logo.png';
import { Link } from 'react-router-dom';

export const NavigationBar = () => {
    return (
        <nav className="navigation-bar">
            <Link to="">
                <img src={Logo} alt="Logo"></img>
            </Link>
            <ul className="navigation-links">
                <NavigationLink title="Product" href="/bubbles" />
                <NavigationLink title="Bundles" href="/bundles" />
                <NavigationLink title="About us" href="/about" />
                <NavigationLink title="Cart" href="/cart" />
            </ul>
        </nav>
    );
}