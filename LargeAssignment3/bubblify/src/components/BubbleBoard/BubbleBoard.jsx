import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

export const BubbleBoard = ({
  id,
  name,
  price,
  image,
}) => {
  return (
    <Card sx={{ width: 250, margin: 2 }}>
      <img src={image} alt={name} />
      <CardContent>
        <Typography gutterBottom variant="h5" component="div">
          {name}
        </Typography>
        <Typography variant="body2" color="text.secondary">
          Price: {price ? price : 0}
        </Typography>
      </CardContent>
      <CardActions>
        <Button size="small">
          <Link 
            to={`/bubbles/${id}`} 
            style={{ textDecoration: 'none' }}>
              More details...
          </Link>
        </Button>
      </CardActions>
    </Card>
  );
}

BubbleBoard.propTypes = {
  // The id of the bubble product
  id: PropTypes.number.isRequired,
  // The name of the bubble product
  name: PropTypes.string.isRequired,
  // The price of the bubble name
  price: PropTypes.number.isRequired,
  // The image of the bubble name
  image: PropTypes.string.isRequired,
}

