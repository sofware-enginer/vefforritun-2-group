import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import PropTypes from 'prop-types';

export const BubbleSmWideBoard = ({
  name,
  price,
  image,
}) => {
  return (
    <Card sx={{ width: 400, height: 120, marginBottom: 1 }} className="bubbleSmWideContent">
      <img src={image} alt={name}/>
      <CardContent className='orderTextContent'>
        <Typography gutterBottom variant="h6" component="div">
          {name}
        </Typography>
        <Typography variant="body2" color="text.secondary">
          Price: {price ? price : 0}
        </Typography>
      </CardContent>
    </Card>
  );
}

BubbleSmWideBoard.propTypes = {
  // The name of the bubble product
  name: PropTypes.string.isRequired,
  // The price of the bubble name
  price: PropTypes.number.isRequired,
  // The image of the bubble name
  image: PropTypes.string.isRequired,
}

