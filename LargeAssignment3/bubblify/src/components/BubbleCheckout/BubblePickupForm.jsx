import { Button } from '@mui/material';
import { useState } from 'react';
import { useForm } from "react-hook-form";
import PropTypes from "prop-types";
import "./styles.css"


export const PickupForm = ({
  onOpen
}) => {
    const [name, setName] = useState('');
    const [telephone, setTelephone] = useState('');
    const{ register,handleSubmit, formState:{errors}}= useForm();

    const onSubmit = () => {
      const userInfoStorage = localStorage.getItem("userInfo");
      const userInfo = JSON.parse(userInfoStorage);
      const userObj = {
        name: name,
        telephone: telephone,
        items: userInfo.items || [],
        totalPrice: userInfo.totalPrice || 0
      };
      localStorage.setItem("userInfo", JSON.stringify(userObj));
      onOpen();
    }

    return (
      <form onSubmit={handleSubmit(onSubmit)}>
        <label>Name:</label>
        <input {...register("name",{required: true,maxLength: 30, pattern: /^[A-Za-z ]+$/i})} type="text" value={name} placeholder="e.g., John Wick" onChange={(e) => setName(e.target.value)}/>
        {errors?.name?.type === "required" && <p>&#x2022; This field is required</p>}
        {errors?.name?.type === "pattern" && <p>&#x2022; Alphabetical characters only</p>}
        <label>Telephone:</label>
        <input {...register("telephone", {required: true, min: 1000000, max: 9999999, valueAsNumber: true, validate: ((value) => value > 0)})} type="tel" value={telephone} placeholder="e.g., 7778899" onChange={(e) => setTelephone(e.target.value)}/>
        {errors?.telephone?.type === "required" && <p>&#x2022; This field is required</p>}
        {errors?.telephone?.type === "validate" && <p>&#x2022; Phone number should be numbers</p>} 
        {errors?.telephone?.type === "max" && <p>&#x2022; Phone number should only be 7 numbers</p>}
        {errors?.telephone?.type === "min" && <p>&#x2022; Phone number should be minimum 1000000 numbers</p>}
        <Button variant="contained" type="submit" sx={{width: 100, marginTop: 2, backgroundColor: "#FF6565", '&:hover': {backgroundColor: "#FAA296"}, alignSelf: "center"}}>Submit</Button>
      </form>
     
    ); 
}

PickupForm.propTypes = {
  // The function which is called when form is submitted
  onOpen: PropTypes.func.isRequired
}

