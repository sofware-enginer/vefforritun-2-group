import { Button } from '@mui/material';
import { useState } from 'react';
import { useForm } from "react-hook-form";
import PropTypes from "prop-types";
import "./styles.css"

export const DeliveryForm = ({
  onOpen,
}) => {
  const [name, setName] = useState("");
  const [address, setAddress] = useState("");
  const [city, setCity] = useState("");
  const [telephone, setTelephone] = useState("");
  const [postalCode, setPostalCode] = useState("");
  const{ register, handleSubmit, formState:{errors} }= useForm();

  const onSubmit = () => {
    const userInfoStorage = localStorage.getItem("userInfo");
    const userInfo = JSON.parse(userInfoStorage);
    const userObj = {
      name: name,
      telephone: telephone,
      address: address,
      city: city,
      postalCode: postalCode,
      items: userInfo.items || [],
      totalPrice: userInfo.totalPrice || 0
    };
    localStorage.setItem("userInfo", JSON.stringify(userObj));
    onOpen();
  }

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <label>Name: </label>
        <input {...register("name",{required: true,maxLength: 30, pattern: /^[A-Za-z]+$/i})} type="text" value={name} placeholder="e.g., John Wick" onChange={(e) => setName(e.target.value)}/>
        {errors?.name?.type === "required" && <p>&#x2022; This field is required</p>}
        {errors?.name?.type === "pattern" && <p>&#x2022; Alphabetical characters only</p>}
      <label>Telephone:</label>
        <input {...register("telephone", {required: true, min: 1000000, max: 9999999, valueAsNumber: true, validate: ((value) => value > 0)})} type="tel" value={telephone} placeholder="e.g., 7778899" onChange={(e) => setTelephone(e.target.value)} />
        {errors?.telephone?.type === "required" && <p>&#x2022; This field is required</p>}
        {errors?.telephone?.type === "validate" && <p>&#x2022; Phone number should be numbers</p>} 
        {errors?.telephone?.type === "max" && <p>&#x2022; Phone number should only be length of 7</p>}
        {errors?.telephone?.type === "min" && <p>&#x2022; Phone number should be minimum 1000000 numbers</p>}
      <label>Address:</label>
        <input {...register("address",{required: true, maxLength: 30})} type="text" value={address} placeholder="e.g., Laugarvegur 12" onChange={(e) => setAddress(e.target.value)} />
        {errors?.address?.type === "required" && <p>&#x2022; This field is required</p>}
      <label>City:</label>
        <input {...register("city",{required: true, maxLength: 30, pattern: /^[A-Za-z]+$/i})} type="text" value={city} placeholder="e.g., Reykjavík" onChange={(e) => setCity(e.target.value)} />
        {errors?.city?.type === "required" && <p>&#x2022; This field is required</p>}
        {errors?.city?.type === "pattern" && <p>&#x2022; Alphabetical characters only</p>}
      <label>Postal Code: </label>
        <input {...register("postalCode",{required: true, valueAsNumber: true, min: 100, max: 999, validate: ((value) => value > 0)})} type="text" value={postalCode} placeholder="e.g., 101" onChange={(e) => setPostalCode(e.target.value)} />
        {errors?.postalCode?.type === "required" && <p>&#x2022; This field is required</p>}
        {errors?.postalCode?.type === "validate" && <p>&#x2022; Postal code should be a numbers</p>} 
        {errors?.postalCode?.type === "min" && <p>&#x2022; Postal code should be at minimum 100</p>}
        {errors?.postalCode?.type === "max" && <p>&#x2022; Postal code should only be length of 3</p>}
      <Button variant="contained" type="submit" sx={{width: 100, marginTop: 2, backgroundColor: "#FF6565", '&:hover': {backgroundColor: "#FAA296"}, alignSelf: "center"}}>Submit</Button>
    </form>
  );
};

DeliveryForm.propTypes = {
  // The function which is called when form is submitted
  onOpen: PropTypes.func.isRequired
}
