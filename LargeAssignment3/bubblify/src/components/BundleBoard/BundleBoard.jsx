import { useState, useEffect } from "react";
import PropTypes from "prop-types";
import "./styles.css";
import { BubbleBoard } from "../BubbleBoard/BubbleBoard";


export const BundleBoard = ({ 
    name, 
    items
}) => {
  const [price, setPrice] = useState(0);
  const [products, setProducts] = useState([]);

  useEffect(() => {
    const product = [];
    const fetchProduct = async () => {
        for (const item of items) {
          const response = await fetch(`http://localhost:3500/api/bubbles/${item}`)
          const data = await response.json();
          product.push(data);
          setProducts(product);
          const totalPrice = product.reduce((total, product) => 
            total + product.price, 0
          );
          setPrice(totalPrice);
    }
  };
  fetchProduct();
  
  }, [items]);


  return (
    <div className="container">
        <h2>{name}</h2>
        <p>Total price: {price}</p>
        <p>Includes:</p>
        <div className="items">
            {products.map((product, key) => {
                return <BubbleBoard key={name + key} id={product.id} name={product.name} price={product.price} image={product.image}></BubbleBoard>
            })}
        </div>
    </div>
  );
};

BundleBoard.propTypes = {
    // The name of the bundles
    name: PropTypes.string.isRequired,
    // The array of bubble products id of the bundles
    items: PropTypes.arrayOf(PropTypes.number.isRequired),
};
