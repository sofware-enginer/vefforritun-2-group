import { Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, Button } from "@mui/material"
import PropTypes from 'prop-types';
import { useEffect, useState } from "react";
import { BubbleSmWideBoard } from "../BubbleSmWideBoard/BubbleSmWideBoard";
import "./styles.css";


export const BubbleOrderModal = ({
    isOpen,
    onClose,
}) => {
    const [ userInfo, setUserInfo ] = useState({});
    const styling = {color: "black", paddingLeft: 10};

    useEffect(() => {
        const storage = localStorage.getItem("userInfo");
        if (storage !== undefined) {
            setUserInfo(JSON.parse(storage));
        }
    }, [isOpen]);

    const confirmCallback = () => {
        const postData = async () => {
            const request = await fetch(`http://localhost:3500/api/orders/${userInfo.telephone}`, {
                method: "POST",
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(userInfo),
            });
            const response = await request.text();
            console.log(response);
        }
        postData();
        userInfo.items = [];
        localStorage.setItem("userInfo", JSON.stringify(userInfo));
        window.location.href = '/confirm';
    }

    return (
        <Dialog open={isOpen} onClose={onClose} fullWidth>
            <DialogTitle fontSize={28}>Order review:</DialogTitle>
            <DialogContent>
                <DialogContentText sx={styling}>
                    Name: {userInfo.name}
                </DialogContentText>
                <DialogContentText sx={styling}>
                    Phone number: {userInfo.telephone}
                </DialogContentText>
                
                <DialogContentText sx={styling}>
                {userInfo.address !== undefined ? `Address: ${userInfo.address}` : null}
                </DialogContentText>
                <DialogContentText sx={styling}>
                    {userInfo.city !== undefined ?
                        `City: ${userInfo.city}` : null
                    }
                </DialogContentText>
                <DialogContentText sx={styling}>
                    {userInfo.postalCode !== undefined ? 
                        `Postal code: ${userInfo.postalCode}` : null
                    }
                </DialogContentText>
                <DialogContentText sx={styling}>
                    {userInfo.items !== undefined ? 
                        "Items: ": null
                    }
                </DialogContentText>
                <div className="orderProducts">
                    {userInfo.items !== undefined ? 
                        userInfo.items.map(item => <BubbleSmWideBoard key={item.id} name={item.name} price={item.price} image={item.image}/>) : null}
                </div>
                <DialogContentText sx={styling}>
                    {userInfo.totalPrice !== undefined ? 
                        `Total price: ${userInfo.totalPrice}` : null
                    }
                </DialogContentText>
            </DialogContent>
            <DialogActions sx={{paddingBottom: 3}}>
                <Button variant="outlined" onClick={() => onClose()} sx={{borderColor: "#FF3B7D", color: "#FF3B7D", width: 130, height: 50, "&:hover": {borderColor: "#FF3B7D", color: "#FFB6B6"}}}>Cancel order</Button>
                <Button variant="outlined" onClick={() => confirmCallback()} sx={{borderColor: "#FF3B7D", color: "#FF3B7D", width: 130, height: 50, "&:hover": {borderColor: "#FF3B7D", color: "#FFB6B6"}}}>
                    Confirm order
                </Button>
            </DialogActions>
        </Dialog>
    );
}

BubbleOrderModal.propTypes = {
    // The boolean value if modal is opened or not
    isOpen: PropTypes.bool.isRequired,
    // The function which is called when the model is closing
    onClose: PropTypes.func.isRequired,
}