import { Button } from "@mui/material";
import { useState, useEffect, useCallback } from "react";
import { BubbleSelectableBoard } from "../../components/BubbleSelectableBoard/BubbleSelectableBoard";
import "./styles.css";

export const BubbleProducts = () => {
  const [products, setProducts] = useState([]);
  const [userInfo, setUserInfo] = useState({});
  const [userPrev, setUserPrev ] = useState({});
  const [selectedCounter, setSelectedCounter] = useState(0);
  
  const changeCounter = (num) => {
    setSelectedCounter(num);
  };

  const getUserLastOrder = () => {
    const storage = localStorage.getItem("userInfo");
    const userObj = JSON.parse(storage);
    const fetchData = async () => { 
      if (userObj !== null && userObj.telephone !== undefined) {
        try {
          const response = await fetch(`http://localhost:3500/api/orders/${userObj.telephone}`);
          const data = await response.json();
          setUserInfo(data);
        } catch (e) {
          console.log(e);
        }
      }
    };
    fetchData();
  };

  const userLastOrderCallback = useCallback(() => {
    const storage = localStorage.getItem("userInfo");
    const userObj = JSON.parse(storage);
    userObj.items = userInfo[userInfo.length-1].items;
    localStorage.setItem("userInfo", JSON.stringify(userObj));
    setUserPrev(userObj);
  }, [userInfo]);

  useEffect( () =>  {
    const fetchData = async () => {
      const response = await fetch("http://localhost:3500/api/bubbles");
      const data = await response.json();
      if (data) {
        setProducts(data);
      }
    }
    fetchData();
    getUserLastOrder();
    changeCounter();
  }, []);

  const handleCheckout = () => {
    window.location.href = '/checkout';
  }

  return (
    <>
      <div className="topContainer">
        <h1 className="content">Products:</h1>
        <div className="buttonContainer">
          <Button disabled={Object.keys(userInfo).length === 0} variant="outlined" className="prevButton" onClick={userLastOrderCallback} sx={{borderColor: "#FF3B7D", color: "#FF3B7D", "&:hover": {borderColor: "#FF3B7D", color: "#FFB6B6"}}}>Use previous order</Button>
          <Button disabled={selectedCounter === 0} variant="outlined" className="checkoutButton" onClick={handleCheckout} sx={{borderColor: "#FF3B7D", color: "#FF3B7D", "&:hover": {borderColor: "#FF3B7D", color: "#FFB6B6"}}}>
             Go to checkout
          </Button>
        </div>
      </div>
      <div className="bubbleList">
        {products.map(product => <BubbleSelectableBoard key={product.id} id={product.id} name={product.name} description={product.description} price={product.price} image={product.image} onCounterChange={changeCounter} onResetSelection/>)}
      </div>
    </>
  );
}
