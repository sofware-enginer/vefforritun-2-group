import React from 'react';
import './styles.css';

export const BubbleHomePage = () => {
    return (
        <>
            <div className='homePage'>
                <h2>Welcome to Bubblify<br/>The ultimate destination for all your bubble needs!</h2>
                <p>
                    At Bubblify, we specialize in providing high-quality bubble-making products that are sure to bring joy and excitement to people of all ages.
                </p>
                <p>
                    Our products are made with the finest materials and are designed to create the biggest and most beautiful bubbles imaginable. Plus, our bubble solution is non-toxic and safe for kids to use, so you can enjoy hours of bubble-filled fun without any worries.
                </p>
                <p>
                    Not only do we offer a wide range of bubble-making products, but we also pride ourselves on our excellent customer service. Our team of knowledgeable and friendly staff is always on hand to answer any questions you may have and help you find the perfect product for your needs.
                </p>
                <p>
                    So whether you're planning a children's party, a family gathering, or just want to add a little bit of bubbly fun to your day, Bubblify has everything you need to make it happen. Shop with us today and let the bubbles begin!
                </p>
            </div>
        </>
    );

}