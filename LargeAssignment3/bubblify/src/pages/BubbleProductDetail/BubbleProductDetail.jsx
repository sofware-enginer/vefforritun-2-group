import { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import "./styles.css";

export const BubbleProductDetails = () => {
  const { bubblesItemId } = useParams();
  const [product, setProduct] = useState(null);

  useEffect(() => {
    const fetchData = async () => {
      const response = await fetch(`http://localhost:3500/api/bubbles/${bubblesItemId}`);
      const data = await response.json();
      setProduct(data);
    }
    fetchData();

  }, [bubblesItemId]);

  if (!product) {
    return <div>Loading...</div>;
  }

  return (
    <div className="detailedBubble">
      <h1>{product.name}</h1>
      <img src={product.image} alt={product.name} />
      <p className="bubbleDescription">{product.description}</p>
      <p>Price: {product.price}</p>
    </div>
  );
}
