import CheckCircleOutlineIcon from "@mui/icons-material/CheckCircleOutline";
import "./styles.css";

export const BubbleConfirmation = () => {
  return (
    <div className="confirmInfo">
      <CheckCircleOutlineIcon sx={{ width: 100, height: 100, color: "green" }} />
      <h1>Congratulations! Your order has been successfully placed! <br /> 🎉🎊🍾🥳</h1>
      <p>
        Thank you for choosing us as your trusted partner in this transaction. We appreciate your business and hope that you'll be satisfied with your purchase.
        Our team is working hard to ensure that your order is processed and shipped as soon as possible.
      </p>
      <p>
        If you have any questions or concerns about your order, please don't hesitate to contact us.
        We're always here to help and make sure that you have a positive experience with us.
      </p>
      <p>
        We're located in the heart of the city and are open 7 days a week. Our staff
        are all bubble experts and are happy to help you find the perfect bubble
        product for your needs.
      </p>
      <p>Once again, thank you for your purchase and we hope to serve you again soon!</p>
    </div>
  )
};

