import { useState, useEffect} from "react";
import { BundleBoard } from "../../components/BundleBoard/BundleBoard";
import "./styles.css";

export const BundleProducts = () => {
  const [bundles, setBundles] = useState([]);

  useEffect(() =>  {
    const fetchData = async () => {
      const response = await fetch("http://localhost:3500/api/bundles")
      const data = await response.json();
      setBundles(data);
    }
    fetchData();
  }, []);

  return (
    <div>
        <h1 className="content">Bundles:</h1>
        {bundles.map(bundle => (
          <BundleBoard key={bundle.name} name={bundle.name} items={bundle.items}/>
        ))}
    </div>
  );
}