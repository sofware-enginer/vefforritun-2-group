import { useState   } from "react";
import { Button, ButtonGroup } from "@mui/material";
import { DeliveryForm } from "../../components/BubbleCheckout/BubbleDeliveryForm.jsx";
import { PickupForm } from "../../components/BubbleCheckout/BubblePickupForm.jsx";
import "./styles.css";
import { BubbleOrderModal } from "../../components/BubbleOrderModal/BubbleOrderModal.jsx";
import { grey } from "@mui/material/colors";

export const BubbleCheckout = () => {
  const [deliveryMethod, setDeliveryMethod] = useState("pickup");
  const [isModalOpen, setIsModalOpen] = useState(false);
  
  return (
    <>
     <h1>Checkout:</h1>
     <div className="deliveryMethodContainer">
        <ButtonGroup variant="secondary" sx={{color: "white"}} aria-label="outlined secondary button group">
          <Button 
            className="button" 
            onClick={() => setDeliveryMethod("pickup")} 
            sx={[deliveryMethod === "pickup" ? {backgroundColor: "#FF6565"} : {backgroundColor: "gray"},{'&:hover': {backgroundColor: "#FAA296"}}]}>
              Pickup
          </Button>
          <Button className="button" 
            onClick={() => setDeliveryMethod("delivery")} 
            sx={[deliveryMethod === "delivery" ? {backgroundColor: "#FF6565"} : {backgroundColor: "gray"}, {'&:hover': {backgroundColor: "#FAA296"}}]}>
              Delivery
          </Button>
        </ButtonGroup>
      </div>
      <div className="formContent">
        {deliveryMethod === "delivery" && <DeliveryForm onOpen={() => setIsModalOpen(true)}/>}
        {deliveryMethod === "pickup" && <PickupForm onOpen={() => setIsModalOpen(true)}/>}
      </div>
      <BubbleOrderModal 
        isOpen={isModalOpen} 
        onClose={() => setIsModalOpen(false)}
      />
    </>
  );
}