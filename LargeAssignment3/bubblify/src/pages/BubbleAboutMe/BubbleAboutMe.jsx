import "./styles.css";

export const About = () =>
 (
    <div className="aboutMe">
      <h1>About Bubblify</h1>
      <p>
        Bubblify is a place where you can find all sorts of bubble-related products,
        from bubble wands and bubble machines to bubble solution and bubble art kits.
      </p>
      <p>
        We're passionate about bubbles and believe that everyone should have the
        opportunity to experience the joy and wonder that bubbles can bring.
      </p>
      <p>
        We're located in the heart of the city and are open 7 days a week. Our staff
        are all bubble experts and are happy to help you find the perfect bubble
        product for your needs.
      </p>
      <p>Come visit us today and let's make some bubbles together!</p>
    </div>
  );

