import { Button } from "@mui/material";
import { useState, useEffect } from "react";
import { BubbleBoard } from "../../components/BubbleBoard/BubbleBoard";
import "./styles.css";


export const BubbleCart = () => {
  const [cartItems, setCartItems] = useState([]);
  const [totalPrice, setTotalPrice] = useState(0);

  // Retrieve cart items from localStorage on component mount
  useEffect(() => {
    const userInfoStorage = localStorage.getItem("userInfo");
    if (userInfoStorage) {
      const userInfo = JSON.parse(userInfoStorage);
      setCartItems(userInfo.items);
      setTotalPrice(userInfo.totalPrice);
    }
  }, []);
  

  const handleCheckout = () => {
    window.location.href = '/checkout';
  };

  return (
    <>
      <div className="infoContainer">
        <h1>Cart Items:</h1>
      {cartItems.length > 0 && (
        <>
          <p>Total price: ${totalPrice || 0}</p>
          <Button variant="outlined" className="button" onClick={handleCheckout} sx={{borderColor: "#FF3B7D", color: "#FF3B7D", "&:hover": {borderColor: "#FF3B7D", color: "#FFB6B6"}}}>Go to checkout</Button>
        </>
      )}
      </div>
      <div className="bubbleCartItems">
        {cartItems.map(cartItem => {
          return (<BubbleBoard key={cartItem.id} id={cartItem.id} name={cartItem.name} price={cartItem.price} image={cartItem.image}/>)
        })}
     </div>
    </>
  );
}
