import { Outlet } from "react-router-dom";
import { NavigationBar } from "../../components/NavigationBar/NavigationBar";

export const MainLayout = () => {
    return (
        <>
            <NavigationBar />
            <div className="page">
                <Outlet/>
            </div>
        </>
    );
}
